<!DOCTYPE html>
<html>

<head>
</head>

<body>
    <?php
    require('animal.php');
    require('Frog.php');
    require('Ape.php');

    $sheep = new Animal("shaun");

    echo $sheep->name; // "shaun"
    echo "<br>";
    echo $sheep->legs; // 2
    echo "<br>";
    echo $sheep->cold_blooded; // false
    echo "<br>";

    echo "<br>";
    $sungokong = new Ape("kera sakti");
    $sungokong->yell();
    echo "<br>";

    echo "<br>";
    $kodok = new Frog("buduk");
    $kodok->jump(); // "hop hop
    echo "<br>";
    ?>
</body>

</html>